# - Check for the presence of NLOPT
#
# The following variables are set when Eigen is found:
#  HAVE_NLOPT     = Set to true, if all components of NLOPT
#                          have been found.
#  NLOPT_INCLUDE_DIR   = Include path for the header files of NLOPT
#  NLOPT_LIBRARIES  = Link these to use NLOPT

## -----------------------------------------------------------------------------
## Check for the header files

find_package(PkgConfig)

find_path (NLOPT_INCLUDE_DIR nlopt.hpp
        PATHS /usr/local/include /usr/include /sw/include /opt/local/include
        )

find_library(NLOPT_LIBRARIES nlopt
        PATHS /usr/local/lib /usr/lib /lib /sw/lib /opt/local/lib $ENV{HOME}/lib
        )

## -----------------------------------------------------------------------------
## Actions taken when all components have been found

if (NLOPT_INCLUDE_DIR AND NLOPT_LIBRARIES)
    set (HAVE_NLOPT TRUE)
else (NLOPT_INCLUDE_DIR AND NLOPT_LIBRARIES)
    if (NOT NLOPT_FIND_QUIETLY)
        if (NOT NLOPT_INCLUDE_DIR)
            message (STATUS "WARNING : Unable to find NLOPT header files !")
        endif (NOT NLOPT_INCLUDE_DIR)
        if (NOT NLOPT_LIBRARIES)
            message (STATUS "WARNING : Unable to find NLOPT header files !")
        endif (NOT NLOPT_LIBRARIES)
    endif (NOT NLOPT_FIND_QUIETLY)
endif (NLOPT_INCLUDE_DIR AND NLOPT_LIBRARIES)

if (HAVE_NLOPT)
    if (NOT NLOPT_FIND_QUIETLY)
        message (STATUS "Found components for NLOPT")
        message (STATUS "NLOPT_INCLUDE_DIR = ${NLOPT_INCLUDE_DIR}")
        message (STATUS "NLOPT_LIBRARIES = ${NLOPT_LIBRARIES}")
    endif (NOT NLOPT_FIND_QUIETLY)
else (HAVE_NLOPT)
    if (NLOPT_FIND_REQUIRED)
        message (FATAL_ERROR "Could not find NLOPT!")
    endif (NLOPT_FIND_REQUIRED)
endif (HAVE_NLOPT)

mark_as_advanced (
        HAVE_NLOPT
        NLOPT_INCLUDE_DIR
        NLOPT_LIBRARIES
)