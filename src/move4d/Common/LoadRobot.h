#ifndef LOADROBOT_H
#define LOADROBOT_H

#include <string>

namespace move4d {

class RobotUrdfLoader
{
public:
    bool loadRobot(const std::string &relativeFilePath);
};

} // namespace move4d

#endif // LOADROBOT_H
