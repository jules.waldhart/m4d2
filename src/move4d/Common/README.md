# Move4dCommon

Move4dCommon provides all basic tools for robotic motion planning:

 - forward kinematics
 - collision detection
 - scene and worldstate representation
 - robot configuration and states

It also provides various tools like logging and more to come.
