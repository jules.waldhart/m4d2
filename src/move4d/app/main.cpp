#include <move4d/Common/LoadRobot.h>
#include <iostream>
#include <move4d/Common/Logging/Logger.h>

using namespace move4d;
int main()
{

    RobotUrdfLoader urdfLoader;
    bool result=urdfLoader.loadRobot("pepper.urdf");
    if(result){
        std::cout<<"ok"<<std::endl;
    }else{
        std::cout<<"FAIL"<<std::endl;
    }

    return 0;
}
